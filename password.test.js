
const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })
  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 20 characters to be true', () => {
    expect(checkLength('12345678901234567890')).toBe(true)
  })
  test('should 21 characters to be false', () => {
    expect(checkLength('123456789012345678901')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('555')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit 1 in password', () => {
    expect(checkDigit('jji1')).toBe(true)
  })
  test('should has not digit in password', () => {
    expect(checkDigit('jji')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password', () => {
    expect(checkSymbol('555!')).toBe(true)
  })
  test('should has not symbol in password', () => {
    expect(checkSymbol('f555')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password Creammer456+ to be true', () => {
    expect(checkPassword('Creammer456+')).toBe(true)
  })
  test('should password Creammer456 to be false', () => {
    expect(checkPassword('Creammer456')).toBe(false)
  })
  test('should password Creammer+ to be false', () => {
    expect(checkPassword('Creammer+')).toBe(false)
  })
  test('should password 112+ to be false', () => {
    expect(checkPassword('112+')).toBe(false)
  })
})
